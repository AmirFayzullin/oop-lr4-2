﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lr4_2_
{
    public partial class Form1 : Form
    {

        IModel<State, int, Action> m = new Model();
        ViewComponent a, b, c;

        public Form1()
        {
            InitializeComponent();
            a = new ViewComponent(textBoxA, numericUpDownA, trackBarA,
                (int x) => m.setState(new Action<int>(ActionType.SET_A, x)), 
                (State s) => s.A, 
                m
            );
            b = new ViewComponent(textBoxB, numericUpDownB, trackBarB,
                (int x) => m.setState(new Action<int>(ActionType.SET_B, x)),
                (State s) => s.B,
                m
            );
            c = new ViewComponent(textBoxC, numericUpDownC, trackBarC,
                (int x) => m.setState(new Action<int>(ActionType.SET_C, x)),
                (State s) => s.C,
                m
            );
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            a = null;
            b = null;
            c = null;
            m = null;
        }
    }
}
