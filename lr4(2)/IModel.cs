﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr4_2_
{
    interface IModel<S, ValueT, Action>
    {
        void setState(Action<ValueT> a);
        S getState();
        void subscribe(EventHandler handler);
        void unsubscribe(EventHandler handler);
    }
}
