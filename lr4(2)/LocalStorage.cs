﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data;

namespace lr4_2_
{
    class LocalStorage
    {
        string filepath = Application.StartupPath + @"\Data.xml";
        State initial;
        StreamReader IStream;
        StreamWriter OStream;
        DataSet ds;
        public LocalStorage(State _initial) {
            initial = _initial;
            if (!File.Exists(filepath)) File.Create(filepath);
            initDS();
        }

        public State load()
        {
            initDS();
            DataRow r = ds.Tables[0].Rows[0];
            return new State(Convert.ToInt32(r[0]), Convert.ToInt32(r[1]), Convert.ToInt32(r[2]));
        }

        public void save(State s)
        {
            DataRow r = ds.Tables[0].Rows[0];
            r[0] = s.A;
            r[1] = s.B;
            r[2] = s.C;
            r.AcceptChanges();
            ds.Tables[0].AcceptChanges();
            ds.AcceptChanges();
            clearXml();
            writeToXml();
        }

        private void writeToXml()
        {
            OStream = new StreamWriter(filepath);
            ds.WriteXml(OStream);
            OStream.Close();
        }

        private void clearXml()
        {
            File.WriteAllText(filepath, string.Empty);
        }

        private void initDS()
        {
            ds = new DataSet();
            try
            {
                loadDSFromXml();
            }
            catch (Exception err)
            {
                fillDS();
            }
        }

        private void loadDSFromXml()
        {
            IStream = new StreamReader(filepath);
            try
            {
                ds.ReadXml(IStream);
            }
            catch (System.Xml.XmlException err)
            {
                throw new Exception();
            }
            IStream.Close();
        }

        private void fillDS()
        {
            ds.DataSetName = "NumbersDS";
            DataTable dt = new DataTable();
            dt.TableName = "Numbers";
            dt.Columns.Add("A", typeof(int));
            dt.Columns.Add("B", typeof(int));
            dt.Columns.Add("C", typeof(int));
            ds.Tables.Add(dt);

            dt.Rows.Add(initial.A, initial.B, initial.C);
        }

        ~LocalStorage()
        {
            ds.Dispose();
        }
    }
}
