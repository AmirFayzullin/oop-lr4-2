﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr4_2_
{
    // presents data readonly stucture for outer world. 
    //It should be inherited by model to define business logic
    class State
    {
        protected int a;
        protected int b;
        protected int c;

        public State()
        {
            a = 0;
            b = 0;
            c = 0;
        }

        public State(int _a, int _b, int _c)
        {
            a = _a;
            b = _b;
            c = _c;
        }

        virtual public int A
        {
            get { return a; }
            protected set { a = value; }
        }
        virtual public int B
        {
            get { return b; }
            protected set { b = value; }
        }
        virtual public int C
        {
            get { return c; }
            protected set { c = value; }
        }
    }
    // enum of ActionTypes for Action
    enum ActionType { SET_A, SET_B, SET_C};
    // presents Action, that is needed for Model to recognize, what should be updated
    struct Action<T>
    {
        public ActionType type;
        public T value;
        public Action(ActionType t, T v)
        {
            type = t;
            value = v;
        }
    }
    class Model : State, IModel<State, int, Action>
    {
        const int min = 0;
        const int max = 100;
        EventHandler subscribers;
        LocalStorage storage;

        public Model()
        {
            storage = new LocalStorage(this as State);
            State sFromLocalStorage = storage.load();
            a = sFromLocalStorage.A;
            b = sFromLocalStorage.B;
            c = sFromLocalStorage.C;
        }

        public Model(int _a, int _b, int _c)
        {
            A = _a;
            B = _b;
            C = _c;
        }

        // provides interface for editing data
        public void setState(Action<int> action)
        {
            switch (action.type)
            {
                case ActionType.SET_A:
                    {
                        A = action.value;
                        break;
                    }
                case ActionType.SET_B:
                    {
                        B = action.value;
                        break;
                    }
                case ActionType.SET_C:
                    {
                        C = action.value;
                        break;
                    }
            }
            subscribers.Invoke(this, null);
        }

        //returns this as State for outer world
        public State getState()
        {
            return this as State;
        }

        public void subscribe(EventHandler handler) {
            subscribers += handler;
        }

        public void unsubscribe(EventHandler handler)
        {
            subscribers -= handler;
        }

        // setters A, B and C define business logic of editing the values
        override public int C
        {
            protected set
            {
                if (value < min || value > max) return;
                c = value;
                if (value < a) a = value;
                if (value < b) b = value;
            }
        }

        override public int B
        {
            protected set {
                if (value < a || value > c || value > max || value < min) return;
                b = value;
            }
        }

        override public int A
        {
            protected set
            {
                if (value < min || value > max) return;
                a = value;
                if (value > c) c = value;
                if (value > b) b = value;
            }
        }

        ~Model()
        {
            storage.save(this as State);
        }
    }
}
