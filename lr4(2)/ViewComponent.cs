﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lr4_2_
{
    class ViewComponent
    {
        public delegate void setStateFn(int x);
        public delegate int mapStateFn(State s);
        TextBox t;
        NumericUpDown n;
        TrackBar tb;
        setStateFn setState;
        mapStateFn mapState;
        IModel<State, int, Action> model;


        public ViewComponent(
            TextBox _t, NumericUpDown _n, TrackBar _tb, 
            setStateFn _setState, mapStateFn _mapState,  
            IModel<State, int, Action> m
            )
        {
            t = _t;
            n = _n;
            tb = _tb;
            setState = _setState;
            mapState = _mapState;
            

            t.KeyUp += tChangeHandler;
            n.ValueChanged += nChangeHandler;
            tb.ValueChanged += tbChangeHandler;
            
            model = m;
            model.subscribe(update);

            fillComponents();
        }

        private void update(object sender, EventArgs e)
        {
            fillComponents();
        }

        private void fillComponents()
        {
            int value = mapState(model.getState());
            t.Text = value.ToString();
            n.Value = value;
            tb.Value = value;
        }

        private void tChangeHandler(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) setState(Convert.ToInt32(t.Text));
        }

        private void nChangeHandler(object sender, EventArgs e)
        {
            setState(Convert.ToInt32(n.Value));
        }

        private void tbChangeHandler(object sender, EventArgs e)
        {
            setState(Convert.ToInt32(tb.Value));
        }

        ~ViewComponent()
        {
            model.unsubscribe(update);
        }
    }
}
